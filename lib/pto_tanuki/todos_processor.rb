# frozen_string_literal: true

module PtoTanuki
  class TodosProcessor
    def initialize(gitlab, group_names)
      @gitlab = gitlab
      @group_names = group_names
      @current_user = gitlab.user
      @mentioned_users = MentionedUsers.new
    end

    def process
      schedule = Schedule.new(@gitlab)
      notifier = Notifier.new(@gitlab, schedule)

      group_ids.each do |group_id|
        @gitlab.todos(group_id: group_id).auto_paginate do |todo|
          if notify?(todo)
            if schedule.out_of_office?
              notifier.notify(todo)
              @mentioned_users.add(todo.author.username)
            else
              notifier.notify(todo, dry: true)
            end
          end
        end
      end
    ensure
      @mentioned_users.save
    end

    def group_ids
      @group_names.map do |name|
        @gitlab.group(name).id
      end
    end

    def notify?(todo)
      return false if @mentioned_users.mentioned?(todo.author.username)

      case todo.action_name
      when 'assigned', 'directly_addressed'
        true
      when 'mentioned'
        username = "@#{@current_user.username}"

        todo.body.include?(username) ||
          todo.target.description.include?(username)
      else
        false
      end
    end
  end
end
