# frozen_string_literal: true
require 'erb'

module PtoTanuki
  class Notifier
    TEMPLATE = <<~NOTE.strip
      @<%= vars[:username] %>

      I am out of office from <%= vars[:start_date] %> until <%= vars[:end_date] %>. Any mentions
      during this period will be ignored, and any todos will be marked as done
      automatically.

      <% if vars[:is_gitlab_project] %>
      For any ~database merge requests for review, please reach
      out to anybody from [`@gl-database`](https://gitlab.com/groups/gl-database/-/group_members) instead.

      We are currently short of database reviewers and maintainers. Please consider participating
      and becoming a database maintainer. More details can be found in https://gitlab.com/gitlab-org/gitlab-ce/issues/63790.
      <% end %>

      <hr>

      :robot: This message was generated automatically using [PTO
      Tanuki](https://gitlab.com/yorickpeterse/pto-tanuki). Future mentions of
      my username will not lead to another notification such as this one, unless
      the list of already notified users gets lost somehow.
    NOTE


    def initialize(gitlab, schedule)
      @gitlab = gitlab
      @schedule = schedule
    end

    def notify(todo, dry: false)
      method =
        if todo.target_type == 'MergeRequest'
          :create_merge_request_note
        elsif todo.target_type == 'Issue'
          :create_issue_note
        end

      return unless method
      return print_body(todo) if dry

      @gitlab.public_send(
        method,
        todo.target.project_id,
        todo.target.iid,
        note_body(todo)
      )
    end

    def print_body(todo)
      puts note_body(todo)
    end

    def note_body(todo)
      vars = {
        username: todo.author.username,
        start_date: @schedule.start_date&.iso8601,
        end_date: @schedule.end_date&.iso8601,
        is_gitlab_project: [13083, 278964].include?(todo.target.project_id),
      }
      ERB.new(TEMPLATE).result(binding)
    end
  end
end
